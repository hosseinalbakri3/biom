package handlers

import (
	"biom/internal/data/phnaddr"
	"biom/internal/data/question"
	"biom/internal/pladform/web"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type phnaddrHandler struct {
	db *gorm.DB
}

func (p phnaddrHandler) Create(w http.ResponseWriter, r *http.Request) {

	sid, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	id, err := strconv.Atoi(sid)
	if err != nil || id <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	pa := phnaddr.NewPhoneAddress{}
	err = json.Unmarshal(body, &pa)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	pa.UID = uint(id)

	//pa validation
	errors := pa.Validate()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	resp, err := phnaddr.Create(p.db, pa, time.Now())
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, resp, http.StatusCreated)

	return
}

func (p phnaddrHandler) Update(w http.ResponseWriter, r *http.Request) {

	//check if id gets from AuthMiddleware
	uids, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	pids := strings.TrimSpace(r.FormValue("id"))
	if pids == "" {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	pid, err := strconv.Atoi(pids)
	if err != nil || pid <= 0 {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	pa, err := phnaddr.One(p.db, pid)
	if err != nil {
		switch err {
		case question.ErrInternal:
			_ = web.RespondErr(w, http.StatusInternalServerError)
			return
		case question.ErrNotFound:
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		default:
			_ = web.RespondErr(w, http.StatusBadRequest)
			return
		}
	}

	if pa.UID != uint(uid) {
		_ = web.RespondErr(w, http.StatusForbidden)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	npa := phnaddr.NewPhoneAddress{}
	err = json.Unmarshal(body, &npa)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	errors := npa.Validate()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	resp, err := phnaddr.Update(p.db, uint(pid), npa)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, resp, http.StatusOK)

}

func (p phnaddrHandler) Delete(w http.ResponseWriter, r *http.Request) {

	//check if id gets from AuthMiddleware
	uids, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	//phnadrr id string = pids
	pids := strings.TrimSpace(r.FormValue("id"))
	if pids == "" {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	pid, err := strconv.Atoi(pids)
	if err != nil || pid <= 0 {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	pa, err := phnaddr.One(p.db, pid)
	if err != nil {
		switch err {
		case question.ErrNotFound:
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		default:
			_ = web.RespondErr(w, http.StatusBadRequest)
			return
		}
	}

	if pa.UID != uint(uid) {
		_ = web.RespondErr(w, http.StatusForbidden)
		return
	}

	err = phnaddr.Delete(p.db, pid)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	resp := struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}{
		Status:  http.StatusOK,
		Message: "deleted",
	}
	_ = web.Respond(w, resp, http.StatusOK)
}
