package handlers

import (
	"biom/internal/data/question"
	"biom/internal/pladform/web"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type questionHandler struct {
	db *gorm.DB
}

//Create func create question for login user
func (q questionHandler) Create(w http.ResponseWriter, r *http.Request) {

	//check if id gets from AuthMiddleware
	idStr, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	id, err := strconv.Atoi(idStr)
	if err != nil || id <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}
	nq := question.NewQuestion{}
	err = json.Unmarshal(body, &nq)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	nq.UID = uint(id)

	//nq validation
	errors := nq.Validate()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	resp, err := question.Create(q.db, nq, time.Now())
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, resp, http.StatusCreated)

}

//Update func update question for login user
func (q questionHandler) Update(w http.ResponseWriter, r *http.Request) {

	//check if id gets from AuthMiddleware
	uids, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	qids := strings.TrimSpace(r.FormValue("id"))
	if qids == "" {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	qid, err := strconv.Atoi(qids)
	if err != nil || qid <= 0 {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	qu, err := question.One(q.db, qid)
	if err != nil {
		switch err {
		case question.ErrInternal:
			_ = web.RespondErr(w, http.StatusInternalServerError)
			return
		case question.ErrNotFound:
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		default:
			_ = web.RespondErr(w, http.StatusBadRequest)
			return
		}
	}

	if qu.UID != uint(uid) {
		_ = web.RespondErr(w, http.StatusForbidden)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	nq := question.NewQuestion{}
	err = json.Unmarshal(body, &nq)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	errors := nq.Validate()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	resp, err := question.Update(q.db, uint(qid), nq)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, resp, http.StatusOK)

}

//Delete func Delete question for login user
func (q questionHandler) Delete(w http.ResponseWriter, r *http.Request) {

	//check if id gets from AuthMiddleware
	uids, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	qids := strings.TrimSpace(r.FormValue("id"))
	if qids == "" {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	qid, err := strconv.Atoi(qids)
	if err != nil || qid <= 0 {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	qus, err := question.One(q.db, qid)
	if err != nil {
		switch err {
		case question.ErrNotFound:
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		default:
			_ = web.RespondErr(w, http.StatusBadRequest)
			return
		}
	}

	if qus.UID != uint(uid) {
		_ = web.RespondErr(w, http.StatusForbidden)
		return
	}

	err = question.Delete(q.db, qid)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	resp := struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}{
		Status:  http.StatusOK,
		Message: "deleted",
	}
	_ = web.Respond(w, resp, http.StatusOK)
}
