package handlers

import (
	"biom/internal/auth"
	"biom/internal/data/phnaddr"
	"biom/internal/data/profile"
	"biom/internal/data/question"
	"biom/internal/data/socialmedia"
	"biom/internal/data/user"
	"biom/internal/pladform/web"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type userHandlers struct {
	db   *gorm.DB
	auth *auth.Auth
}

func (u userHandlers) userDetail(w http.ResponseWriter, r *http.Request) {

	resp := struct {
		Profile     *profile.Profile          `json:"user"`
		PhnAddr     []phnaddr.PhoneAddress    `json:"phone_address"`
		Question    []question.Question       `json:"question"`
		SocialMedia []socialmedia.SocialMedia `json:"social_media"`
	}{}

	un , ok := r.Context().Value("user_name").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
	}

	prof, err := profile.One(u.db, un)
	if err != nil {
		if err == profile.ErrNotFound {
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		} else if err == profile.ErrInternal {
			_ = web.RespondErr(w, http.StatusInternalServerError)
			return
		}
	}

	resp.Profile = &prof

	if resp.Profile == nil {
		_ = web.RespondErr(w, http.StatusNotFound)
		return
	}

	pa, err := phnaddr.List(u.db, prof.UID)
	if err != nil {
		log.Println(err)
	}
	resp.PhnAddr = pa

	q, err := question.List(u.db, prof.UID)
	if err != nil {
		log.Println(err)
	}
	resp.Question = q

	sm, err := socialmedia.List(u.db, prof.UID)
	if err != nil {
		log.Println(err)
	}
	resp.SocialMedia = sm
	_ = web.Respond(w, resp, 200)

}

func (u userHandlers) login(w http.ResponseWriter, r *http.Request) {

	bs , err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w,http.StatusBadRequest)
		return
	}

	nu := user.LoginUser{}
	err = json.Unmarshal(bs, &nu)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	errors := nu.Validation()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}
	us, err := user.OneByEmail(u.db, nu.Email)
	if err != nil {
		_ = web.RespondErr(w, http.StatusNotFound)
		return
	}

	//we do that because of attack
	//we check if user enter password 20 times wrong in last 10
	//counter is between 1 - 20
	if us.Counter >= 20 && time.Now().Sub(us.UpdatedAt) < time.Minute * 5 {
		_ = web.RespondMessageErr(w , " err too many req " , http.StatusTooManyRequests)
		return
	}

	if time.Now().Sub(us.UpdatedAt) > time.Minute * 5 {
		_ = user.UpdateCounter(u.db, us.ID, 1)
	}

	err = bcrypt.CompareHashAndPassword([]byte(us.HashPassword), []byte(nu.Password))
	if err != nil {
		m := map[string]string {"password" : "err pass"}
		if us.Counter < 20 {
			_ = user.UpdateCounter(u.db, us.ID, us.Counter+1)
		}
		_ = web.RespondMessageErr(w,m,http.StatusForbidden)
		return
	}

	//claims for token
	clim := auth.Claims{
		StandardClaims: jwt.StandardClaims{Id: strconv.FormatUint(uint64(us.ID), 10)},
		Email:          us.Email,
	}

	//Generate new Token
	token, err := auth.GenerateNewToken(u.auth, clim)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	//Response Struct
	resp := struct {
		Token    string `json:"token"`
		Email    string `json:"email"`
	}{
		Token:    token,
		Email:    us.Email,
	}

	_ = web.Respond(w, resp, http.StatusOK)

}

func (u userHandlers) signUp(w http.ResponseWriter, r *http.Request) {

	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	nu := user.NewUser{}
	err = json.Unmarshal(bs, &nu)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	//Validating User Input for singUp
	errors := nu.Validation()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	us, err := user.CreateUserWithProfile(u.db, nu, time.Now())
	if err != nil {

		switch err {

		case user.ErrDuplicateEmail:
			resp := map[string]string {"email" : err.Error()}
			_ = web.RespondMessageErr(w, resp,http.StatusBadRequest)
			return

		case profile.ErrDuplicateUserName:
			resp := map[string]string {"username" : err.Error()}
			_ = web.RespondMessageErr(w, resp,http.StatusBadRequest)
			return

		default:
			_ = web.RespondErr(w, http.StatusInternalServerError)
			return
		}
	}

	//Claims for token
	clim := auth.Claims{
		StandardClaims: jwt.StandardClaims{Id: strconv.FormatUint(uint64(us.ID), 10)},
		Email:          us.Email,
	}

	//Generating token
	token, err := auth.GenerateNewToken(u.auth, clim)
	if err != nil {
		_ = user.Delete(u.db, us.ID)
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	//Response Struct
	resp := struct {
		Token    string `json:"token"`
		Email    string `json:"email"`
	}{
		Token:    token,
		Email:    us.Email,
	}

	_ = web.Respond(w, resp, http.StatusCreated)

}

func (u userHandlers) forgetPassword (w http.ResponseWriter , r *http.Request) {

	email := strings.TrimSpace(r.FormValue("email"))
	if email == "" {
		_ = web.RespondErr(w , http.StatusBadRequest)
		return
	}

	_, _ = user.ForgetPassCode(u.db, email)


}

func (u userHandlers) resetPassword(w http.ResponseWriter , r *http.Request) {

	uidStr , ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid , err := strconv.Atoi(uidStr)
	if err != nil {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w , http.StatusInternalServerError)
		return
	}

	rp := user.ResetPassword{}
	err = json.Unmarshal(bs, &rp)
	if err != nil {
		_ = web.RespondErr(w , http.StatusBadRequest)
		return
	}

	errors := rp.Validation()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	usr, err := user.OneByID(u.db, uint(uid))
	if err != nil {
		switch err {
		case user.ErrInternal:
			_ = web.RespondErr(w,http.StatusInternalServerError)
			return

		case user.ErrNotFound:
			_ = web.RespondErr(w,http.StatusForbidden)
			return
		}
	}

	err = bcrypt.CompareHashAndPassword([]byte(usr.HashPassword), []byte(rp.Password))
	if err != nil {
		resp := map[string]string{"password" : "wrong password"}
		_ = web.RespondMessageErr(w, resp, http.StatusBadRequest)
		return
	}

	err = user.UpdatePassword(u.db, uint(uid), rp)
	if err != nil {
		_ = web.RespondErr(w , http.StatusInternalServerError)
		return
	}


	resp := map[string]string{"message" : "successfully changed"}
	_ = web.Respond(w , resp , http.StatusOK)
}
