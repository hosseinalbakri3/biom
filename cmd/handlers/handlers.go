package handlers

import (
	"biom/internal/auth"
	"biom/internal/mid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"log"
	"net/http"
)

func API(mainDB *gorm.DB,viewDB * gorm.DB, a *auth.Auth) {

	router := mux.NewRouter()

	u := userHandlers{
		db:   mainDB,
		auth: a,
	}


	r :=router.PathPrefix("/").Subrouter()
	r.Use(mid.ViewMiddleware(mainDB,viewDB))
	r.Methods("GET").Path("/{user_name}").HandlerFunc(u.userDetail)


	//accounts api for user apis
	//this apis do not need token and auth middleware
	account := router.PathPrefix("/api/accounts").Subrouter()
	account.Methods("POST").Path("/signup").HandlerFunc(u.signUp)
	account.Methods("POST").Path("/login").HandlerFunc(u.login)


	//sub router for apis
	api := router.PathPrefix("/api").Subrouter()
	api.Use(mid.AuthMiddleware(a))

	//accounts api that need token
	api.Methods("PUT").Path("/accounts/password/reset").HandlerFunc(u.resetPassword)
	

	//question crud
	q := questionHandler{
		db: mainDB,

	}
	api.Methods("POST").Path("/question/create").HandlerFunc(q.Create)
	api.Methods("PUT").Path("/question/update").HandlerFunc(q.Update)
	api.Methods("DELETE").Path("/question/delete").HandlerFunc(q.Delete)

	//socialmedia crud
	s := socialHandler{
		db: mainDB,
	}
	api.Methods("POST").Path("/socialmedia/create").HandlerFunc(s.Create)
	api.Methods("PUT").Path("/socialmedia/update").HandlerFunc(s.Update)
	api.Methods("PUT").Path("/socialmedia/rows/update").HandlerFunc(s.UpdateRows)
	api.Methods("DELETE").Path("/socialmedia/delete").HandlerFunc(s.Delete)

	//phone address crud
	pa := phnaddrHandler{
		db: mainDB,
	}

	api.Methods("POST").Path("/phnaddr/create").HandlerFunc(pa.Create)
	api.Methods("PUT").Path("/phnaddr/update").HandlerFunc(pa.Update)
	api.Methods("DELETE").Path("/phnaddr/delete").HandlerFunc(pa.Delete)

	//profile
	p := profileHandler{
		db: mainDB,
		viewDB : viewDB,
	}
	api.Methods("GET").Path("/profile").HandlerFunc(p.profile)
	api.Methods("POST").Path("/profile/picture").HandlerFunc(p.profilePicture)
	api.Methods("GET").Path("/profile/viewers").HandlerFunc(p.profileViewers)
	api.Methods("PUT").Path("/profile/update").HandlerFunc(p.profileUpdate)

	err := http.ListenAndServe("localhost:8081", router)
	if err != nil {
		log.Panic(err)
	}
}
