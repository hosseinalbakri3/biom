package handlers

import (
	"biom/internal/data/question"
	"biom/internal/data/socialmedia"
	"biom/internal/pladform/web"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type socialHandler struct {
	db *gorm.DB
}

func (s socialHandler) Create(w http.ResponseWriter, r *http.Request) {

	sid, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}
	id, err := strconv.Atoi(sid)
	if err != nil || id <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	ns := socialmedia.NewSocialMedia{}
	err = json.Unmarshal(body, &ns)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	ns.UID = uint(id)

	//sm validation
	errors := ns.Validate()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	resp, err := socialmedia.Create(s.db, ns, time.Now())
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, resp, http.StatusCreated)

	return
}

func (s socialHandler) Update(w http.ResponseWriter, r *http.Request) {

	//check if id gets from AuthMiddleware
	uids, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	sids := strings.TrimSpace(r.FormValue("id"))
	if sids == "" {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	sid, err := strconv.Atoi(sids)
	if err != nil || sid <= 0 {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	sm, err := socialmedia.One(s.db, sid)
	if err != nil {
		switch err {
		case question.ErrInternal:
			_ = web.RespondErr(w, http.StatusInternalServerError)
			return
		case question.ErrNotFound:
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		default:
			_ = web.RespondErr(w, http.StatusBadRequest)
			return
		}
	}

	if sm.UID != uint(uid) {
		_ = web.RespondErr(w, http.StatusForbidden)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	nsm := socialmedia.NewSocialMedia{}
	err = json.Unmarshal(body, &nsm)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	errors := nsm.Validate()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	resp, err := socialmedia.Update(s.db, uint(sid), nsm)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, resp, http.StatusOK)

}

func (s socialHandler) Delete(w http.ResponseWriter, r *http.Request) {

	//check if id gets from AuthMiddleware
	uids, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	sids := strings.TrimSpace(r.FormValue("id"))
	if sids == "" {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	sid, err := strconv.Atoi(sids)
	if err != nil || sid <= 0 {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	social, err := socialmedia.One(s.db, sid)
	if err != nil {
		switch err {
		case question.ErrNotFound:
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		default:
			_ = web.RespondErr(w, http.StatusBadRequest)
			return
		}
	}

	if social.UID != uint(uid) {
		_ = web.RespondErr(w, http.StatusForbidden)
		return
	}

	err = socialmedia.Delete(s.db, sid)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	resp := struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}{
		Status:  http.StatusOK,
		Message: "deleted",
	}
	_ = web.Respond(w, resp, http.StatusOK)
}

func (s socialHandler) UpdateRows(w http.ResponseWriter , r *http.Request)  {

	uids, ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	var items []int
	err = json.Unmarshal(body, &items)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	err = socialmedia.UpdateRowOrder(s.db, uint(uid), items)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	resp := map[string]string {"message" : "change successfully"}
	_ = web.Respond(w, resp, http.StatusOK)
}
