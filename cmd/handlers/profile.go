package handlers

import (
	"biom/internal/data/phnaddr"
	"biom/internal/data/profile"
	"biom/internal/data/question"
	"biom/internal/data/socialmedia"
	"biom/internal/data/view"
	"biom/internal/pladform/web"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type profileHandler struct {
	db *gorm.DB
	viewDB *gorm.DB

}

func (p profileHandler) profile(w http.ResponseWriter, r *http.Request) {

	resp := struct {
		Profile     *profile.Profile          `json:"user"`
		PhnAddr     []phnaddr.PhoneAddress    `json:"phone_address"`
		Question    []question.Question       `json:"question"`
		SocialMedia []socialmedia.SocialMedia `json:"social_media"`
	}{}

	uids ,ok :=r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	prof, err := profile.OneByUserId(p.db, uint(uid))
	if err != nil {
		if err == profile.ErrNotFound {
			_ = web.RespondErr(w, http.StatusNotFound)
			return
		} else if err == profile.ErrInternal {
			_ = web.RespondErr(w, http.StatusInternalServerError)
			return
		}
	}

	resp.Profile = &prof

	if resp.Profile == nil {
		_ = web.RespondErr(w, http.StatusNotFound)
		return
	}

	pa, err := phnaddr.List(p.db, prof.UID)
	if err != nil {
		log.Println(err)
	}
	resp.PhnAddr = pa

	q, err := question.List(p.db, prof.UID)
	if err != nil {
		log.Println(err)
	}
	resp.Question = q

	sm, err := socialmedia.List(p.db, prof.UID)
	if err != nil {
		log.Println(err)
	}
	resp.SocialMedia = sm
	_ = web.Respond(w, resp, 200)

}

func (p profileHandler) profileUpdate(w http.ResponseWriter , r *http.Request)  {

	uids ,ok :=r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	np := profile.NewProfile{}
	err = json.Unmarshal(body, &np)
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	errors := np.Validation()
	if errors != nil {
		_ = web.RespondMessageErr(w, errors, http.StatusBadRequest)
		return
	}

	pr ,err := profile.Update(p.db, uint(uid), np)
	if err != nil {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, pr, http.StatusOK)

}

func (p profileHandler) profilePicture(w http.ResponseWriter , r *http.Request){

	uids ,ok :=r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid, err := strconv.Atoi(uids)
	if err != nil || uid <= 0 {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	f , fh, err := r.FormFile("profilePicture")
	if err != nil {
		_ = web.RespondErr(w, http.StatusBadRequest)
		return
	}

	image,err := profile.SaveProfile(p.db,f ,fh ,uid)
	if err != nil {
		switch err {
		case profile.ErrInvalidFormat:
			_ = web.RespondErr(w, http.StatusUnsupportedMediaType)
			return

		case profile.ErrSize:
			_ = web.RespondErr(w, http.StatusRequestEntityTooLarge)
			return

		default:
			_ = web.RespondErr(w, http.StatusInternalServerError)
			return

		}
	}

	resp := struct {
		ProfileUrl string
	}{
		ProfileUrl: image,
	}

	_ = web.RespondMessageErr(w, resp, http.StatusOK)

}

func (p profileHandler) profileViewers(w http.ResponseWriter , r *http.Request)  {

	uidStr , ok := r.Context().Value("id").(string)
	if !ok {
		_ = web.RespondErr(w, http.StatusInternalServerError)
		return
	}

	uid , err := strconv.Atoi(uidStr)
	if err != nil {
		_ = web.RespondErr(w, http.StatusUnauthorized)
		return
	}

	items, err := view.List(p.viewDB, uint(uid))
	if err != nil {
		_ = web.RespondErr(w , http.StatusInternalServerError)
		return
	}

	_ = web.Respond(w, items, http.StatusOK)

}


