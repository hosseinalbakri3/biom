package main

import (
	"biom/cmd/handlers"
	auth "biom/internal/auth"
	"biom/internal/data/view"
	"biom/internal/pladform/database"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
)

func main() {

	log.Println("Start")

	a := auth.Auth{
		Key:       "hereiskeyforaithandthereisnopoblemwithit",
		Algorithm: "HS256",
	}

	// main database config
	cfg := database.Config{
		Scheme:   "postgres",
		User:     "mrb",
		Password: "hsin0272",
		Host:     "localhost",
		Port:     "5432",
		Name:     "mydb",
	}

	//main database open
	db, err := database.Open(cfg)
	if err != nil {
		panic(" cannot connect to database main")
		return
	}

	err = database.PrepareTablesMainDB(db)
	if err != nil {
		log.Panic(err)
	}

	err = database.Seed(db, &a)
	if err != nil {
		log.Println("error in seeding")
	}

	//ViewDB
	cfg = database.Config{
		Scheme:   "postgres",
		User:     "mrb",
		Password: "hsin0272",
		Host:     "localhost",
		Port:     "5432",
		Name:     "view",
	}

	//main database open
	dbview , err := database.Open(cfg)
	if err != nil {
		panic(" cannot connect to database view ")
		return
	}

	dbview.AutoMigrate(view.View{})

	handlers.API(db , dbview, &a)

}
