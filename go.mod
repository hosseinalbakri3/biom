module biom

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20200817114649-df4adffc9d8c // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.15
	github.com/lib/pq v1.1.1
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/api v0.32.0 // indirect
)
