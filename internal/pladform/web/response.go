package web

import (
	"encoding/json"
	"net/http"
)

func Respond(w http.ResponseWriter, data interface{}, statusCode int) error {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(statusCode)

	bs, err := json.Marshal(&data)
	if err != nil {
		return err
	}

	_, err = w.Write(bs)
	if err != nil {
		return err
	}

	return nil
}

func RespondErr(w http.ResponseWriter, statusCode int) error {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	resp := struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}{}

	resp.Status = statusCode

	switch statusCode {
	case http.StatusNotFound:
		resp.Message = ""
	case http.StatusRequestEntityTooLarge:
		resp.Message = ""
	case http.StatusTooManyRequests:
		resp.Message = ""
	case http.StatusUnsupportedMediaType:
		resp.Message = ""
	case http.StatusForbidden:
		resp.Message = ""
	case http.StatusBadRequest:
		resp.Message = ""
	case 500:
		resp.Message = ""
	default:
		resp.Message = ""
	}

	err := Respond(w, resp, statusCode)

	if err != nil {
		return err
	}

	return nil
}

func RespondMessageErr(w http.ResponseWriter, errMess interface{}, statusCode int) error {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	if errMess == nil {
		err := RespondErr(w, statusCode)
		return err
	}

	resp := struct {
		Status  int         `json:"status"`
		Message interface{} `json:"message"`
	}{}

	resp.Status = statusCode
	resp.Message = errMess

	err := Respond(w, resp, statusCode)

	if err != nil {
		return err
	}

	return nil
}
