package database

import (
	"biom/internal/auth"
	"biom/internal/data/phnaddr"
	"biom/internal/data/profile"
	"biom/internal/data/question"
	"biom/internal/data/socialmedia"
	"biom/internal/data/user"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"strconv"
	"time"
)

func Seed(db *gorm.DB, au *auth.Auth) error {
	nu := user.NewUser{
		Email:    "hoseinalbakri@gmail.com",
		Password: "password",
	}
	u, err := user.Create(db, nu, time.Now())
	if err != nil {
		return err
	}

	//
	prof := profile.NewProfile{
		UID:        u.ID,
		UserName:   "hossein",
		Status:     "its status",
		Bio:        "its bio",
	}

	_, _ = profile.Create(db, prof, time.Now())

	clim := auth.Claims{
		StandardClaims: jwt.StandardClaims{Id: strconv.FormatUint(uint64(u.ID), 10)},
		Email:          u.Email,
	}
	s, _ := auth.GenerateNewToken(au, clim)
	fmt.Println(s)
	//
	sm := socialmedia.NewSocialMedia{
		UID:   u.ID,
		Title: "کانال تلگرام",
		Link:  "telegram",
		Kind:  "telegram",
	}

	for i := 0; i < 2; i++ {
		_, _ = socialmedia.Create(db, sm, time.Now())
	}

	//
	qs := question.NewQuestion{
		UID:      u.ID,
		Question: "qusetion",
		Answer:   "answer",
	}

	for i := 0; i < 2; i++ {
		qs.Question = fmt.Sprint("quesrion ", i)
		_, _ = question.Create(db, qs, time.Now())
	}

	//
	pa := phnaddr.NewPhoneAddress{
		UID:     u.ID,
		Lat:     "32.66",
		Lon:     "51.65",
		Number:  "0903347080",
		Address: "iran ahvaz ",
	}

	for i := 0; i < 2; i++ {
		pa.Address = fmt.Sprint("iran ahvaz ", i)
		_, _ = phnaddr.Create(db, pa, time.Now())
	}

	return nil
}
