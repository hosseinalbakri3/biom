package database

import (
	"biom/internal/data/phnaddr"
	"biom/internal/data/profile"
	"biom/internal/data/question"
	"biom/internal/data/socialmedia"
	"biom/internal/data/user"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"net/url"

	_ "github.com/lib/pq"
)

// Config is the required properties to use the database.
type Config struct {
	Scheme   string
	User     string
	Password string
	Host     string
	Port     string
	Name     string
}

// Open knows how to open a database connection based on the configuration.
func Open(cfg Config) (*gorm.DB, error) {

	q := make(url.Values)
	q.Set("sslmode", "disable")

	u := url.URL{
		User:     url.UserPassword(cfg.User, cfg.Password),
		Host:     fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
		Scheme:   cfg.Scheme,
		Path:     cfg.Name,
		RawQuery: q.Encode(),
	}
	db, err := gorm.Open(cfg.Scheme, u.String())
	if err != nil {
		return nil, err
	}

	return db, nil
}

//prepareTables create tables if tables not exist in MainDB
func PrepareTablesMainDB(db *gorm.DB) error {
	if db == nil {
		return errors.New("db is nil")
	}

	db.Table(user.Table).AutoMigrate(&user.User{})

	//create table questions
	db.Table(question.Table).AutoMigrate(&question.Question{})
	db.Exec("ALTER TABLE " + question.Table + " ADD FOREIGN KEY (uid) REFERENCES users(ID) ON DELETE CASCADE;")

	//create table social media
	db.Table(socialmedia.Table).AutoMigrate(&socialmedia.SocialMedia{})
	db.Exec("ALTER TABLE " + socialmedia.Table + " ADD FOREIGN KEY (uid) REFERENCES users(ID) ON DELETE CASCADE;")

	//create table phone address
	db.Table(phnaddr.Table).AutoMigrate(&phnaddr.PhoneAddress{})
	db.Exec("ALTER TABLE " + phnaddr.Table + " ADD FOREIGN KEY (uid) REFERENCES users(ID) ON DELETE CASCADE;")

	//create table profile
	db.Table(profile.Table).AutoMigrate(&profile.Profile{})
	db.Exec("ALTER TABLE " + profile.Table + " ADD FOREIGN KEY (uid) REFERENCES users(ID) ON DELETE CASCADE;")

	db.Exec("CREATE OR REPLACE FUNCTION trigger_set_timestamp() " +
		"RETURNS TRIGGER AS $$ " +
		"BEGIN " +
		"  NEW.updated_at = NOW();" +
		"  RETURN NEW;" +
		"END; " +
		"$$ LANGUAGE plpgsql;")

	db.Exec("CREATE TRIGGER set_timestamp BEFORE UPDATE ON " + user.Table + " FOR EACH ROW EXECUTE PROCEDURE trigger_set_timestamp(); ")
	db.Exec("CREATE TRIGGER set_timestamp BEFORE UPDATE ON " + question.Table + " FOR EACH ROW EXECUTE PROCEDURE trigger_set_timestamp(); ")
	db.Exec("CREATE TRIGGER set_timestamp BEFORE UPDATE ON " + socialmedia.Table + " FOR EACH ROW EXECUTE PROCEDURE trigger_set_timestamp(); ")
	db.Exec("CREATE TRIGGER set_timestamp BEFORE UPDATE ON " + phnaddr.Table + " FOR EACH ROW EXECUTE PROCEDURE trigger_set_timestamp(); ")
	db.Exec("CREATE TRIGGER set_timestamp BEFORE UPDATE ON " + profile.Table + " FOR EACH ROW EXECUTE PROCEDURE trigger_set_timestamp(); ")

	return nil
}

//prepareTables create tables if tables not exist in ViewDB
func PrepareTablesViewDB()  {

}



