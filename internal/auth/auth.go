package auth

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
)

var (
	ErrAuthenticationFailure = errors.New("authentication failed")
)

type Claims struct {
	jwt.StandardClaims
	Email    string
}

type Auth struct {
	Key       string
	Algorithm string
}

// GenerateToken generates a signed JWT token string representing the user Claims.
func GenerateNewToken(a *Auth, c Claims) (string, error) {
	method := jwt.GetSigningMethod(a.Algorithm)
	//jwt.SigningMethodHS256
	token := jwt.NewWithClaims(method, &c)
	tokenString, err := token.SignedString([]byte(a.Key))
	if err != nil {
		return tokenString, err
	}
	return tokenString, err
}

// ValidateToken recreates the Claims that were used to generate a token. It
// verifies that the token was signed using our key.
func ValidateToken(a *Auth, tokenStr string) (Claims, error) {

	f := func(token *jwt.Token) (i interface{}, e error) {
		return []byte(a.Key), nil
	}

	var claims Claims
	token, err := jwt.ParseWithClaims(tokenStr, &claims, f)
	if err != nil {
		return Claims{}, ErrAuthenticationFailure
	}

	if !token.Valid {
		return Claims{}, ErrAuthenticationFailure
	}

	return claims, nil

}
