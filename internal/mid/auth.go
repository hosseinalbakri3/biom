package mid

import (
	"biom/internal/auth"
	"biom/internal/pladform/web"
	"context"
	"net/http"
)

func AuthMiddleware(a *auth.Auth) func(next http.Handler) http.Handler {

	f := func(next http.Handler) http.Handler {

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token := r.Header.Get("token")

			if token == "" {
				_ = web.RespondErr(w, http.StatusUnauthorized)
				return
			}

			claim, err := auth.ValidateToken(a, token)
			if err != nil {
				_ = web.RespondErr(w, http.StatusUnauthorized)
				return
			}
			ctx := context.WithValue(r.Context(), "id", claim.Id)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
	return f
}
