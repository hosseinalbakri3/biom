package mid

import (
	"biom/internal/data/profile"
	"biom/internal/data/view"
	"biom/internal/pladform/web"
	"context"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"net/http"
	"time"
)

func ViewMiddleware(mainDB *gorm.DB , viewDB *gorm.DB) func(next http.Handler) http.Handler {

	f := func(next http.Handler) http.Handler {

		return http.HandlerFunc(func(w http.ResponseWriter ,r *http.Request) {
			
			vars := mux.Vars(r)

			un, ok := vars["user_name"]
			if !ok {
				_ = web.RespondErr(w, http.StatusBadRequest)
				return
			}

			ctx := context.WithValue(r.Context() , "user_name" , un)

			p , err := profile.One(mainDB, un)
			if err != nil {
				next.ServeHTTP(w , r.WithContext(ctx))
				return
			}

			_ = view.Insert(viewDB, p.UID , time.Now())
			next.ServeHTTP(w , r.WithContext(ctx))
		})
	}

	return f
}
