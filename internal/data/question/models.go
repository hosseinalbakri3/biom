package question

import (
	"time"
)

const Table = "questions"

//Question is an item and contains Q&A
//each user can have multiple Q&A
type Question struct {
	ID        uint      `json:"id,omitempty" gorm:"primary_key"`
	UID       uint      `json:"-" gorm:"not null"`
	Question  string    `json:"question" gorm:"type:text;not null"`
	Answer    string    `json:"answer" gorm:"type:text;not null"`
	CreatedAt time.Time `json:"-" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `json:"-" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP"`
}

//NewQuestion is what we require from user to add question
type NewQuestion struct {
	UID      uint   `json:"uid,omitempty"`
	Question string `json:"question"`
	Answer   string `json:"answer"`
}
