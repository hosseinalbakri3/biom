package question

import validation "github.com/go-ozzo/ozzo-validation"

func (q NewQuestion) Validate() map[string]string {

	qv := validation.Validate(&q.Question, validation.Required.Error("q e"),
		validation.NotNil.Error("q n"),
		validation.Length(1, 512).Error("q l"))

	av := validation.Validate(&q.Answer, validation.Required.Error("a e"),
		validation.NotNil.Error("a n"),
		validation.Length(1, 512).Error("a l"))

	errors := validation.Errors{
		"question": qv,
		"answer":   av,
	}

	err := map[string]string{}
	for key, value := range errors {
		if value != nil {
			err[key] = value.Error()
		}
	}

	if len(err) == 0 {
		return nil
	}

	return err
}
