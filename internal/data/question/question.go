package question

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"time"
)

var (
	// ErrNotFound is used when a specific Product is requested but does not exist.
	ErrNotFound = errors.New("not found")

	// ErrInvalidID occurs when an ID is not in a valid form.
	ErrInvalidID = errors.New("ID is not in its proper form")

	// ErrForbidden occurs when a user tries to do something that is forbidden to them according to our access control policies.
	ErrForbidden = errors.New("attempted action is not allowed")

	//ErrInternal occurs when sql error happened or in generating some internal code
	ErrInternal = errors.New("internal error happened")
)

func Create(db *gorm.DB, nq NewQuestion, now time.Time) (Question, error) {

	q := Question{
		UID:       nq.UID,
		Question:  nq.Question,
		Answer:    nq.Answer,
		CreatedAt: now,
		UpdatedAt: now,
	}

	if result := db.Table(Table).Create(&q); result.Error != nil {
		return Question{}, ErrInternal
	}
	fmt.Println(q)
	return q, nil
}

func List(db *gorm.DB, uid uint) ([]Question, error) {
	var qs []Question

	if result := db.Table(Table).Where("uid = ? ", uid).Find(&qs); result.Error != nil {

		if result.RecordNotFound() {
			return qs, ErrNotFound
		}
		return qs, ErrInternal
	}

	return qs, nil
}

func One(db *gorm.DB, id int) (Question, error) {
	var q Question

	if result := db.Table(Table).Where("id = ? ", id).First(&q); result.Error != nil {
		if result.RecordNotFound() {
			return q, ErrNotFound
		}
		return q, ErrInternal
	}

	return q, nil
}

func Delete(db *gorm.DB, id int) error {
	if result := db.Table(Table).Where("id = ? ", id).Delete(&Question{}); result.Error != nil {
		return ErrInternal
	}
	return nil
}

func Update(db *gorm.DB, id uint, nq NewQuestion) (Question, error) {
	q := Question{
		Question: nq.Question,
		Answer:   nq.Answer,
	}

	result := db.Table(Table).Where("id = ?", id).Update(map[string]interface{}{
		"question": nq.Question,
		"answer":   nq.Answer})
	if result.Error != nil {
		return Question{}, ErrInternal
	}

	return q, nil
}
