package user

import "time"

const (
	Table     = "users"
	RoleAdmin = "ADMIN"
	RoleUser  = "USER"
)

// ProfileUrl represents someone with access to our system.
type User struct {
	ID           uint      `json:"id" gorm:"primary_key"`
	Email        string    `json:"email" gorm:"type:text;not null;unique"`
	HashPassword string    `json:"-" gorm:"type:text;not null"`
	Role         string    `json:"-" gorm:"type:varchar(8);default:'USER'"`
	Counter      int       `json:"-" gorm:"not null"`
	Code         int		`json:"-"`
	CodeExpire   time.Time	`json:"-"`
	CreatedAt    time.Time `json:"-" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt    time.Time `json:"-" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP"`
}

// NewUser contains information needed to create a new ProfileUrl.
type NewUser struct {
	ID       uint   `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	UserName string `json:"username"`
}


//UpdatePassword contains data needed for update user password
type ResetPassword struct {
	Password string `json:"password"`
	PasswordConfirm string `json:"password_confirm"`
}

//ProfileUser contains information for both profile and user tables
type ProfileUser struct {
	ID       uint   `json:"id"`
	UserName string `json:"username"`
	Email    string `json:"email"`
}

//LoginUser contains information for login
type LoginUser struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
