package user

import (
	"biom/internal/data/profile"
	"errors"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"strings"
	"time"
)

var (
	// ErrNotFound is used when a specific ProfileUrl is requested but does not exist.
	ErrNotFound = errors.New("not found")

	// ErrDuplicateEmail occurs when a user wanna duplicate email value violates unique constraint .
	ErrDuplicateEmail = errors.New("error duplicate email")

	//ErrInternal occurs when sql error happened or in generating some internal code
	ErrInternal = errors.New("internal error happened")

	//Table is name of table in database
)

func Create(db *gorm.DB, nu NewUser, now time.Time) (User, error) {

	var u User

	hash, err := bcrypt.GenerateFromPassword([]byte(nu.Password), bcrypt.DefaultCost)
	if err != nil {
		return u, ErrInternal
	}

	u = User{
		Email:        nu.Email,
		HashPassword: string(hash),
		Counter:      0,
		CreatedAt:    now,
		UpdatedAt:    now,
	}

	if result := db.Table(Table).Create(&u); result.Error != nil {
		if strings.Contains(result.Error.Error(), "duplicate") &&
			strings.Contains(result.Error.Error(), "users_email_key") {
			return User{} , ErrDuplicateEmail
		}
		return User{}, ErrInternal
	}

	return u, nil
}

func CreateUserWithProfile(db *gorm.DB, nu NewUser, now time.Time) (NewUser, error) {

	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return NewUser{}, ErrInternal
	}

	u, err := Create(tx, nu, now)
	if err != nil {
		tx.Rollback()
		return NewUser{}, err
	}

	np := profile.NewProfile{
		UID:      u.ID,
		UserName: nu.UserName,
	}

	_, err = profile.Create(tx, np, now)
	if err != nil {
		tx.Rollback()
		return NewUser{}, err
	}

	if result := tx.Commit(); result.Error != nil {
		return NewUser{}, ErrInternal
	}

	nu.ID = u.ID

	return nu, nil
}

func OneByEmail(db *gorm.DB, email string) (User, error) {
	var user User

	if result := db.Table(Table).Where("email = ? ", &email).First(&user); result.Error != nil {
		if result.RecordNotFound() {
			return user, ErrNotFound
		}
		return user, ErrInternal
	}

	return user, nil
}

func OneByID(db *gorm.DB, id uint) (User, error) {
	var user User

	if result := db.Table(Table).Where("id = ? ", &id).First(&user); result.Error != nil {
		if result.RecordNotFound() {
			return user, ErrNotFound
		}
		return user, ErrInternal
	}

	return user, nil
}

func Delete(db *gorm.DB, id uint) error {
	if result := db.Table(Table).Where("id = ? ", id).Delete(&User{}); result.Error != nil {
		return ErrInternal
	}
	return nil
}

func UpdateCounter(db *gorm.DB , id uint,counter int) error {

	u := User{
		Counter: counter,
	}

	if result := db.Table(Table).Where("id = ?", id).Update(&u); result.Error != nil {
		return ErrInternal
	}

	return nil
}

//ForgetPassCode func generate new code for forget password and save it in users
func ForgetPassCode(db *gorm.DB , email string) (int , error){

	code := rand.Intn(89999)+10000
	time := time.Now().Add(5 * time.Minute)

	u := User{
		Code:         code,
		CodeExpire:   time,
	}

	if result := db.Table(Table).Where(" email = ? ").Update(&u) ; result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return  0 , ErrNotFound
		}

		return 0 , ErrInternal
	}

	return code, nil
}

func UpdatePassword(db *gorm.DB, id uint, rp ResetPassword)error{

	hash, err := bcrypt.GenerateFromPassword([]byte(rp.PasswordConfirm), bcrypt.DefaultCost)
	if err != nil {
		return ErrInternal
	}

	u := User{
		HashPassword: string(hash),
	}


	if result := db.Table(Table).Where("id = ?", id).Update(&u); result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return ErrNotFound
		}
		return ErrInternal
	}

	return nil
}


