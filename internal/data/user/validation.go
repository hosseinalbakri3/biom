package user

import (
	"errors"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"strings"
)

//Validation NewUser email and password
func (nu NewUser) Validation() map[string]string {
	ue := validation.Validate(&nu.Email, is.Email.Error("u e"), validation.Required.Error(" e r"))

	up := validation.Validate(&nu.Password, is.ASCII.Error("p a"),
		validation.Length(5, 64).Error("s"),
		validation.Required.Error(" e r"))

	uun := validation.Validate(&nu.UserName, is.ASCII.Error("pun assci") ,validation.Required.Error(" e r"),
		validation.Length(2, 64))

	errors := validation.Errors{
		"email":    ue,
		"password": up,
		"username": uun,
	}

	err := map[string]string{}
	for key, value := range errors {
		if value != nil {
			err[key] = value.Error()
		}
	}

	if len(err) == 0 {
		return nil
	}

	return err
}

//Validation LoginUser
func (lu LoginUser) Validation() map[string]string {

	ue := validation.Validate(&lu.Email , is.Email.Error("u e") , validation.Required.Error("r e"))

	up := validation.Validate(lu.Password , is.ASCII.Error("p a"),
		validation.Length(5,64).Error("l e"),
		validation.Required.Error(" e r") , validation.By(spaceChecker))


	errors := validation.Errors{
		"email" : ue ,
		"password" :up ,
	}

	err := map[string]string{}
	for key , value := range errors {
		if value != nil {
			err[key] = value.Error()
		}
	}

	if len(err) == 0 {
		return nil
	}

	return err
}

//Validation ResetPassword

func (rp ResetPassword) Validation() map[string]string{

	cp := validation.Validate(rp.PasswordConfirm, is.ASCII.Error("p a"),
		validation.Length(5, 64).Error("s"),
		validation.Required.Error(" e r") , validation.By(spaceChecker))

	errors := validation.Errors{
		"confirm_password": cp,
	}

	err := map[string]string{}
	for key, value := range errors {
		if value != nil {
			err[key] = value.Error()
		}
	}

	if len(err) == 0 {
		return nil
	}

	return err
}

//spaceChecker is function for checking password not contains space
func spaceChecker(value interface{}) error  {
	s, _ := value.(string)
	if strings.Contains(s , " ") {
		return errors.New("password should not contains space")
	}

	return nil
}
