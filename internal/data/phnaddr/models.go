package phnaddr

import (
	"time"
)

const Table = "phone_addresses"

//PhoneAddress is item contains phone number and address of user
//each user can has multiple address and number
type PhoneAddress struct {
	ID        uint      `json:"id,omitempty" gorm:"primary_key"`
	UID       uint      `json:"-" gorm:"not null"`
	Lat       string    `json:"lat"`
	Lon       string    `json:"lon"`
	Number    string    `json:"number" gorm:"type:varchar(12)"`
	Title     string    `json:"title" gorm:"type:varchar(64)"`
	Address   string    `json:"address" gorm:"type:text;not null"`
	CreatedAt time.Time `json:"-" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `json:"-" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP"`
}

type NewPhoneAddress struct {
	UID     uint   `json:"uid,omitempty"`
	Title   string  `json:"title" gorm:"type:varchar(64)"`
	Lat     string `json:"lat"`
	Lon     string `json:"lon"`
	Number  string `json:"number"`
	Address string `json:"address"`
}
