package phnaddr

import (
	"errors"
	"github.com/jinzhu/gorm"
	"time"
)

var (
	// ErrNotFound is used when a specific Product is requested but does not exist.
	ErrNotFound = errors.New("not found")

	//ErrInternal occurs when sql error happened or in generating some internal code
	ErrInternal = errors.New("internal error happened")
)

func Create(db *gorm.DB, nq NewPhoneAddress, now time.Time) (PhoneAddress, error) {
	pa := PhoneAddress{
		UID:       nq.UID,
		Lat:       nq.Lat,
		Lon:       nq.Lon,
		Number:    nq.Number,
		Address:   nq.Address,
		CreatedAt: now,
		UpdatedAt: now,
	}

	if result := db.Table(Table).Create(&pa); result.Error != nil {
		return PhoneAddress{}, ErrInternal
	}

	return pa, nil
}

func One(db *gorm.DB, id int) (PhoneAddress, error) {
	var pa PhoneAddress
	if result := db.Table(Table).Where("id = ?", id).First(&pa); result.Error != nil {
		if result.RecordNotFound() {
			return pa, ErrNotFound
		}
		return pa, ErrInternal
	}
	return pa, nil
}

func List(db *gorm.DB, uid uint) ([]PhoneAddress, error) {
	var pa []PhoneAddress

	if result := db.Table(Table).Where("uid = ? ", uid).Find(&pa); result.Error != nil {

		if result.RecordNotFound() {
			return pa, ErrNotFound
		}
		return pa, ErrInternal
	}

	return pa, nil
}

func Delete(db *gorm.DB, id int) error {
	if result := db.Table(Table).Where("id = ? ", id).Delete(&PhoneAddress{}); result.Error != nil {
		return ErrInternal
	}
	return nil
}

func Update(db *gorm.DB, id uint, np NewPhoneAddress) (PhoneAddress, error) {

	p := PhoneAddress{
		Lat:     np.Lat,
		Lon:     np.Lon,
		Number:  np.Number,
		Address: np.Address,
	}

	result := db.Table(Table).Where("id = ? ", id).
		Update(map[string]interface{}{
			"number":  np.Number,
			"lat":     np.Lat,
			"lon":     np.Lon,
			"address": np.Address})

	if result.Error != nil {
		return PhoneAddress{}, ErrInternal
	}

	return p, nil
}
