package phnaddr

import (
	"errors"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func (np NewPhoneAddress) Validate() map[string]string {

	f := func(value interface{}) error {
		s, _ := value.(NewPhoneAddress)
		if s.Address == "" && s.Number == "" {
			return errors.New("both cannot be empty")
		}

		if len(s.Address) < 5 || len(s.Address) > 256 {
			return errors.New("add len")
		}

		return nil
	}

	pt := validation.Validate(&np.Lat , validation.Length(0 , 64).Error("len"))
	plat := validation.Validate(&np.Lat, is.Latitude.Error("lon"))
	plon := validation.Validate(&np.Lon, is.Longitude.Error("lat"))
	pn := validation.Validate(&np.Number, is.Digit.Error("digit"))
	pa := validation.Validate(np, validation.By(f))

	errors := validation.Errors{
		"lat":     plat,
		"lon":     plon,
		"number":  pn,
		"address": pa,
		"title": pt,
	}

	err := map[string]string{}
	for key, value := range errors {
		if value != nil {
			err[key] = value.Error()
		}
	}

	if len(err) == 0 {
		return nil
	}

	return err

}
