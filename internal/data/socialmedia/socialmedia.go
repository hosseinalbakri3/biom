package socialmedia

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"time"
)

var (
	// ErrNotFound is used when a specific Product is requested but does not exist.
	ErrNotFound = errors.New("not found")

	// ErrInvalidID occurs when an ID is not in a valid form.
	ErrNoRowAffected = errors.New("no row affected")

	// ErrForbidden occurs when a user tries to do something that is forbidden to them according to our access control policies.
	ErrForbidden = errors.New("attempted action is not allowed")

	//ErrInternal occurs when sql error happened or in generating some internal code
	ErrInternal = errors.New("internal error happened")
)

func Create(db *gorm.DB, nq NewSocialMedia, now time.Time) (SocialMedia, error) {

	sm := SocialMedia{
		UID:       nq.UID,
		Title:     nq.Title,
		Link:      nq.Link,
		Kind:      nq.Kind,
		CreatedAt: now,
		UpdatedAt: now,
	}

	sml, _ := LastByRow(db, nq.UID)
	fmt.Println(sml)
	sm.Row=sml.Row+1

	fmt.Println(nq.UID)
	if result := db.Table(Table).Create(&sm); result.Error != nil {
		return SocialMedia{}, ErrInternal
	}


	return sm, nil
}

func List(db *gorm.DB, uid uint) ([]SocialMedia, error) {
	var sm []SocialMedia

	if result := db.Table(Table).Where("uid = ? ", uid).Order("row").Find(&sm); result.Error != nil {

		if result.RecordNotFound() {
			return sm, ErrNotFound
		}
		return sm, ErrInternal
	}

	return sm, nil
}

func Update(db *gorm.DB, id uint, media NewSocialMedia) (SocialMedia, error) {
	sm := SocialMedia{
		Title: media.Title,
		Link:  media.Link,
		Kind:  media.Kind,
	}

	result := db.Table(Table).Where("id = ?", id).Update(map[string]interface{}{
		"title": sm.Title,
		"link":  sm.Link,
		"kind":  sm.Kind})
	if result.Error != nil {
		return SocialMedia{}, ErrInternal
	}

	return sm, nil
}

func One(db *gorm.DB, id int) (SocialMedia, error) {
	var sm SocialMedia
	if result := db.Table(Table).Where("id = ?", id).Or("row").First(&sm); result.Error != nil {
		if result.RecordNotFound() {
			return sm, ErrNotFound
		}
		return sm, ErrInternal
	}

	return sm, nil
}

func LastByRow(db *gorm.DB, id uint) (SocialMedia, error) {
	var sm SocialMedia
	if result := db.Table(Table).Where("uid = ?", id).Order("row desc").First(&sm); result.Error != nil {
		if result.RecordNotFound() {
			return sm, ErrNotFound
		}
		return sm, ErrInternal
	}

	return sm, nil
}

func Delete(db *gorm.DB, id int) error {
	if result := db.Table(Table).Where("id = ? ", id).Delete(&SocialMedia{}); result.Error != nil {
		return ErrInternal
	}
	return nil
}

//UpdateRowOrder is for updating row column for specific user
//example of query that used :
//update test as t set
//    column_a = c.column_a
//from (values
//    ('123', 1),
//    ('345', 2)
//) as c(column_b, column_a)
//where c.column_b = t.column_b;
func UpdateRowOrder(db * gorm.DB , id uint,sm []int) error{

	query := "update "+Table+" as q set "+
    			"row = r.row "+
				"from ( values "

	for i := 0 ; i< len(sm) ; i++ {
		if i == len(sm)-1 {
			query += fmt.Sprintf("( %v , %v )",sm[i] ,i+1)
		}else {
			query += fmt.Sprintf("( %v , %v ),",sm[i] ,i+1)
		}
	}

	query+= fmt.Sprintf(" ) as r ( id, row ) where r.id = q.id and q.uid = %v" , id)

	r := db.Exec(query)
	if r.RowsAffected == 0 {
		return ErrNoRowAffected
	}

	return nil
}
