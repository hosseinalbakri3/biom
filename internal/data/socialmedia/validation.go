package socialmedia

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func (ns NewSocialMedia) Validate() map[string]string {

	st := validation.Validate(&ns.Title, validation.Length(0, 128).Error(" "))
	sk := validation.Validate(&ns.Kind, validation.In(smKinds...).Error("k n"),
		validation.Required.Error("k r"))
	sl := validation.Validate(&ns.Link, validation.Required.Error(" "), is.URL.Error("url"))

	errors := validation.Errors{
		"kind":  sk,
		"link":  sl,
		"title": st,
	}

	err := map[string]string{}
	for key, value := range errors {
		if value != nil {
			err[key] = value.Error()
		}
	}

	if len(err) == 0 {
		return nil
	}

	return err
}
