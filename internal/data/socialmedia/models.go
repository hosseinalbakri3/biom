package socialmedia

import (
	"time"
)

const (
	Table     = "social_media"
	Telegram  = "telegram"
	Instagram = "instagram"
	YouTube   = "youtube"
	WhatsApp  = "whatsapp"
	WebSite   = "website"
	Twitter   = "twitter"
	FaceBook  = "facebook"
	Email     = "email"
	LinkedIn  = "linked_in"
)

var smKinds = []interface{}{Telegram, Instagram, YouTube, WhatsApp, WebSite, Twitter, FaceBook, Email, LinkedIn}

//SocialMedia is user web base communication
type SocialMedia struct {
	ID        uint      `json:"id,omitempty" gorm:"primary_key"`
	UID       uint      `json:"-" gorm:"not null"`
	Title     string    `json:"title" gorm:"type:varchar(128);not null"`
	Link      string    `json:"link" gorm:"type:text;not null"`
	Kind      string    `json:"kind" gorm:"type:varchar(128);not null"`
	Row       int		`json:"-" gorm:"not null"`
	CreatedAt time.Time `json:"-" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `json:"-" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP"`
}

//NewSocialMedia is what we want from user
type NewSocialMedia struct {
	UID   uint   `json:"uid,omitempty"`
	Title string `json:"title"`
	Link  string `json:"link"`
	Kind  string `json:"kind"`
}

type UpdateRows struct {
	ID   uint   `json:"id"`
	Row  int	`json:"row"`
}
