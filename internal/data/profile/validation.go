package profile

import (
	"errors"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"strings"
)

func (np NewProfile) Validation() map[string]string {
	pun := validation.Validate(np.UserName, is.ASCII.Error("pun assci"),
		validation.Length(2, 64) ,
		validation.By(spaceChecker))
	pb := validation.Validate(&np.Bio, validation.Length(0, 1024).Error("bio len"))
	ps := validation.Validate(&np.Status, validation.Length(0, 64))

	errors := validation.Errors{
		"username": pun,
		"bio":      pb,
		"status":   ps,
	}

	err := map[string]string{}
	for key, value := range errors {
		if value != nil {
			err[key] = value.Error()
		}
	}

	if len(err) == 0 {
		return nil
	}

	return err

}

//spaceChecker is function for checking password not contains space
func spaceChecker(value interface{}) error  {
	s, _ := value.(string)
	if strings.Contains(s , " ") {
		return errors.New("password should not contains space")
	}

	return nil
}
