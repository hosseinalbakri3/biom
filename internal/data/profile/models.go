package profile

import "time"

const Table = "profiles"

// ProfileUrl id detail of user page
type Profile struct {
	ID         uint      `json:"id" gorm:"primary_key"`
	UID        uint      `json:"-" gorm:"not null;unique"`
	UserName   string    `json:"username" gorm:"type:varchar(64);not null;unique"`
	Status     string    `json:"status" gorm:"type:varchar(128)"`
	Bio        string    `json:"bio" gorm:"type:varchar(1024);"`
	ProfileUrl string    `json:"profile" gorm:"type:varchar(512)"`
	CreatedAt  time.Time `json:"-" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt  time.Time `json:"-" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP"`
}

// NewProfile contains information needed to create a new ProfileUrl.
type NewProfile struct {
	UID        uint   `json:"uid"`
	UserName   string `json:"username"` //64
	Status     string `json:"status"`   //64
	Bio        string `json:"bio"`      //1024
}
