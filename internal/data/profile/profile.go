package profile

import (
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/nfnt/resize"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"path"
	"strings"
	"time"
)

var (
	// ErrNotFound is used when a specific Product is requested but does not exist.
	ErrNotFound = errors.New("not found")

	// ErrInvalidID occurs when an ID is not in a valid form.
	ErrInvalidID = errors.New("ID is not in its proper form")

	// ErrForbidden occurs when a user tries to do something that is forbidden to them according to our access control policies.
	ErrForbidden = errors.New("attempted action is not allowed")

	// ErrDuplicateEmail occurs when a user wanna duplicate email value violates unique constraint .
	ErrDuplicateUserName = errors.New("error duplicate username")

	//ErrInternal occurs when sql error happened or in generating some internal code
	ErrInternal = errors.New("internal error happened")

	//
	ErrInvalidFormat = errors.New("wrong image format")

	//
	ErrSize = errors.New("big size")
)

func Create(db *gorm.DB, np NewProfile, now time.Time) (Profile, error) {

	p := Profile{
		UID:       np.UID,
		UserName:  np.UserName,
		Status:    np.Status,
		Bio:       np.Bio,
		CreatedAt: now,
		UpdatedAt: now,
	}

	if result := db.Table(Table).Create(&p); result.Error != nil{

		if strings.Contains(result.Error.Error(), "duplicate") &&
			strings.Contains(result.Error.Error(), "profiles_user_name_key"){
			return Profile{} , ErrDuplicateUserName
		}

		return Profile{}, ErrInternal
	}

	return p, nil
}

func One(db *gorm.DB, un string) (Profile, error) {

	var p Profile

	if result := db.Table(Table).Where("user_name = ?", un).First(&p); result.Error != nil {

		if result.RecordNotFound() {
			return p, ErrNotFound
		}

		return p, ErrInternal
	}

	return p, nil
}

func OneByUserId(db *gorm.DB, uid uint) (Profile, error) {

	var p Profile

	if result := db.Table(Table).Where("uid = ?", uid).First(&p); result.Error != nil {

		if result.RecordNotFound() {
			return p, ErrNotFound
		}

		return p, ErrInternal
	}

	return p, nil
}

func Update(db *gorm.DB, id uint, np NewProfile) (Profile,error) {

	sm := Profile{
		UserName: np.UserName,
		Status:   np.Status,
		Bio:      np.Bio,
	}

	if result := db.Table(Table).Where("id = ?", id).Update(&sm); result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return Profile{},ErrNotFound
		}
		return Profile{},ErrInternal
	}

	return sm ,nil
}

func SaveProfile (db *gorm.DB,f multipart.File, fh *multipart.FileHeader , id int) (string ,error) {

	if float64(fh.Size) / (1024 * 1024) > 10 {
		return "" , ErrSize
	}
	pName, err := GenerateProfilePictureName(f, ".jpg")
	if err != nil {
		return "",ErrInternal
	}

	pBytes, err := ioutil.ReadAll(f)
	if err != nil {
		return "",ErrInternal
	}

	profImage, _, err := image.Decode(bytes.NewReader(pBytes))
	if err != nil {
		//maybe we need make another custom type
		if err == image.ErrFormat {
			return "",ErrInvalidFormat
		}
		return "",ErrInternal
	}

	reImage := resize.Resize(600 , 0 , profImage, resize.Bilinear)

	mainDir , err := os.Getwd()
	if err != nil {
		return "",ErrInternal
	}

	picPath := path.Join(mainDir , "static/image" , pName)

	file, err := os.Create(picPath)
	if err != nil {
		return "",ErrInternal
	}
	defer file.Close()

	err = jpeg.Encode(file, reImage, nil)
	if err != nil {
		return "",ErrInternal
	}

	profile := Profile{
		UID:        uint(id),
		ProfileUrl: pName,
	}

	if result := db.Table(Table).Where("uid = ? " , id).Update(&profile) ; result.Error != nil {
		return "",ErrInternal
	}

	return pName,nil
}

func GenerateProfilePictureName(file multipart.File , format string) (string , error){
	sha := sha256.New()

	_, err := io.Copy(sha, file)
	if err !=nil {
		return "", nil
	}
	_, _ = file.Seek(0, 0)

	return fmt.Sprintf("%X",sha.Sum(nil))+ format, nil
}
