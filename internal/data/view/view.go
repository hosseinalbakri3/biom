package view

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"time"
)

var (
	//ErrInternal occurs when sql error happened or in generating some internal code
	ErrInternal = errors.New("internal error happened")
)

func Insert(db *gorm.DB , id uint , t time.Time) error {

	v := View{
		UID:  id,
		Time: t ,
	}

	if result := db.Table(Table).Create(&v) ; result.Error != nil  {
		return result.Error
	}

	return nil
}

func List(db *gorm.DB ,id uint) ([]ViewItem , error)  {


	 s := "with days as (select generate_series (" +
		"date_trunc('day',now() at time zone 'asia/tehran') - '1 week - 1day'::interval ," +
		"date_trunc('day',now() at time zone 'asia/tehran')," +
		"'1 day'::interval ) as day)" +
	 	"select days.day , count(views.time) from days left join views " +
	 	"on date_trunc('day' ,views.time at time zone 'asia/tehran')=days.day and views.uid=%d group by 1 order by days.day;"

	 query := fmt.Sprintf(s,id)

	 var vl []ViewItem


	if result := db.Raw(query).Scan(&vl) ; result.Error != nil {
		 return vl , ErrInternal
	 }

	 return vl , nil
}
