package view

import "time"

const Table = "views"

//View is user page views
type View struct {
	ID        uint      `json:"id,omitempty" gorm:"primary_key"`
	UID       uint      `json:"-" gorm:"not null"`
	Time      time.Time `json:"time" gorm:"type:TIMESTAMPTZ;not null;default:CURRENT_TIMESTAMP"`
}

type ViewItem struct {
	Day time.Time
	Count int

}



